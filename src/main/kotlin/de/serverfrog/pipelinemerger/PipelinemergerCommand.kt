package de.serverfrog.pipelinemerger

import picocli.CommandLine.Command
import picocli.CommandLine.Option
import java.nio.file.Files
import java.nio.file.Paths
import javax.inject.Inject

@Command(
        name = "pipelinemerger", description = ["This tool helps to merge multiple GitLab CI Pipelines Artifacts"],
        mixinStandardHelpOptions = true,
        subcommands = [DownloadSingleFileCommand::class]
)
class PipelinemergerCommand : Runnable {

    override fun run() {
        println("Please execute a Subcommand")
    }


}
