package de.serverfrog.pipelinemerger

import io.micronaut.configuration.picocli.PicocliRunner

class Application {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            PicocliRunner.run(PipelinemergerCommand::class.java, *args)
        }
    }
}