package de.serverfrog.pipelinemerger

import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Artifact
import org.gitlab4j.api.models.Job
import org.gitlab4j.api.models.Pipeline
import org.gitlab4j.api.models.PipelineStatus
import java.io.InputStream
import java.nio.file.Path
import java.util.stream.Collectors
import javax.inject.Singleton


@Singleton
class GitlabApi {

    private lateinit var gitLabApi: GitLabApi

    private var verbose: Boolean = false

    fun init(url: String, token: String, verbose: Boolean) {
        gitLabApi = GitLabApi(url, token)
        this.verbose = verbose
    }

    fun getArtifacts(projectIdOrPath: String, jobName: String, filter: Map<String, String>): MutableList<Artifact>? {
        val job = findJob(projectIdOrPath, jobName, filter)
        return job.artifacts
    }

    fun downloadArtifactsFile(
        projectIdOrPath: String, jobName: String, artifactPath: Path
        , filter: Map<String, String>
    ): InputStream {
        val job = findJob(projectIdOrPath, jobName, filter)

        return gitLabApi.jobApi.downloadSingleArtifactsFile(projectIdOrPath, job.id, artifactPath)
    }


    private fun findJob(projectIdOrPath: String, jobName: String, filter: Map<String, String>): Job {
        // Get the list of projects your account has access to
        val project = gitLabApi.projectApi.getProject(projectIdOrPath)
        println("Checking Project: ${project.webUrl}")

        val pipelines = gitLabApi.pipelineApi.getPipelines(project.id)
        val collect = pipelines.stream()
            .filter { pipeline -> pipeline.status == PipelineStatus.SUCCESS }
            .filter { pipeline -> filterPipeline(filter, pipeline) }
            .sorted { pipeline1, pipeline2 -> pipeline2.id - pipeline1.id }
            .collect(Collectors.toList())
        println("Have ${collect.size} Successful Pipeline(s). Take latest one")

        if (collect.size == 0) {
            throw RuntimeException("Found no pipline with given input")
        }

        val latestPipeline = collect[0]
        println("Last Pipeline finished at ${latestPipeline.finished_at}. ${latestPipeline.webUrl}")

        val jobsForPipeline = gitLabApi.jobApi.getJobsForPipeline(project.id, latestPipeline.id)
        println("Found ${jobsForPipeline.size} job(s) for the given pipeline")

        val runnedJob = jobsForPipeline.stream()
            .filter { job -> job.name == jobName }
            .findFirst()
            .get()
        println("Job has ${runnedJob.artifacts.size} Artifact(s). ${runnedJob.webUrl}")
        return runnedJob
    }


    private fun filterPipeline(filter: Map<String, String>, pipeline: Pipeline): Boolean {
        if (filter.isEmpty()) return true

        val convertPipelineToMap = convertPipelineToMap(pipeline)

        for ((key, value) in convertPipelineToMap.entries) {
            val filterValue = filter[key] ?: continue

            if (value.contains(filterValue)) {
                return true
            }
        }
        return false
    }

    private fun convertPipelineToMap(pipeline: Pipeline): Map<String, String> {
        val jsonRep = pipeline.toString().trim()
        val lines = jsonRep.lines()
        val filteredLines = lines.stream()
            .filter { line -> !line.contains('{') }
            .filter { line -> !line.contains('}') }
            .collect(Collectors.toList())
        val map = mutableMapOf<String, String>()
        filteredLines.stream()
            .forEach { p -> map.putAll(mapOf(lineToKeyValue(p))) }
        return map
    }

    private fun lineToKeyValue(line: String): Pair<String, String> {
        val indexOf = line.indexOf(':')

        var key = line.substring(0, indexOf)
        var value = line.substring(indexOf + 1)
        key = key.trim().removeSurrounding("\"")
        value = value.trim().removeSuffix(",").removeSurrounding("\"")

        return Pair(key, value)
    }

}