package de.serverfrog.pipelinemerger

import picocli.CommandLine
import javax.inject.Inject

abstract class AbstractCommand: Runnable {

    @CommandLine.Option(names = ["-v", "--verbose"], description = ["..."])
    var verbose: Boolean = false

    @CommandLine.Option(required = true, names = ["-t", "--token"], description = ["Access Token from GitLab with api privileges"])
    lateinit var  accessToken: String

    @CommandLine.Option(required = true, names = ["-u", "--url"], description = ["Address of the GitLab Server"])
    lateinit var url: String

    @CommandLine.Option(required = true, names = ["-p", "--project"], description = ["Path or ID of the GitLab Project"])
    lateinit var project: String

    @CommandLine.Option(required = true, names = ["-j", "--job"], description = ["Name of the GitLab CI Job"])
    lateinit var jobName: String

    @Inject
    lateinit var glApi: GitlabApi

    override fun run() {
        glApi= GitlabApi()
        glApi.init(url, accessToken, verbose)
        execute()
    }

    abstract fun execute()
}