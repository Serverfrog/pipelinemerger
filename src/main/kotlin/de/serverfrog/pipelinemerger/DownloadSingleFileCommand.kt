package de.serverfrog.pipelinemerger

import picocli.CommandLine
import picocli.CommandLine.ParentCommand
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths


@CommandLine.Command(
        name = "Download Single File", description = ["This command downloads a single File from a successful runned job"],
        aliases = ["downloadSingleFile"],
        mixinStandardHelpOptions = true
)
class DownloadSingleFileCommand : AbstractCommand() {

    @ParentCommand
    private lateinit var parent: PipelinemergerCommand


    @CommandLine.Option(required = true, names = ["-f", "--filePath"], description = ["Path to the File"])
    private var filePath: Path = Paths.get(".")


    @CommandLine.Option(required = true, names = ["-d", "--destination"], description = ["Path where the File should be downloaded"])
    private var destination: Path = Paths.get(".")

    @CommandLine.Option(names = ["--filter"], description = ["Filter based on FIELD=contains"])
    private var filter: Map<String, String> = mutableMapOf()

    override fun execute() {
        val downloadArtifactsFile = glApi.downloadArtifactsFile(project, jobName, filePath, filter)
        val readBytes = downloadArtifactsFile.readBytes()
        Files.write(destination, readBytes)
    }

}