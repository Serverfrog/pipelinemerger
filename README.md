# PipelineMerger

This Project is to merge Artifacts from multiple Gitlab CI Pipelines into one single Artifact.

## Features
It is possible to get the most current successful build Artifact from an Project.
It can be Filtered by branch and stage
Another feature is it to use a configuration file to fetch multiple Projects
